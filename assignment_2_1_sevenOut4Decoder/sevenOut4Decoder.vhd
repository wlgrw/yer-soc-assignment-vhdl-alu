--------------------------------------------------------------------
--! \file      lsevenOut4Decoder.vhd
--! \date      see top of 'Version History'
--! \brief     7 segment decoder with dot-driver and extended characters
--! \author    Remko Welling (WLGRW) remko.welling@han.nl
--! \copyright HAN TF ELT/ESE Arnhem 
--!
--! Version History:
--! ----------------
--!
--! Nr: |Date:      |Author: |Remarks:
--! ----|-----------|--------|-----------------------------------
--! 001 |25-2-2015  |WLGRW   |Inital version
--! 002 |28-3-20159 |WLGRW   |Modification to set dot in display
--! 003 |17-10-2019 |WLGRW   |Updated for use in LOGCIR labs
--! 004 |22-12-2019 |WLGRW   |added constants for all numbers and signs
--! 005 |25-11-2020 |WLGRW   |Updated for H-EHE-SOC class
--! 
--! Design:
--! -------
--! Figure 2 presents the input-output diagram of teh 7 out of 4 decoder.
--! 
--! \verbatim
--!
--!  Figure 2: Input-output diagram of the display driver.
--! 
--!              +---------+
--!          4   |         |
--! input ---/---|         |
--!              |         |   7
--!   dot -------|         |---/--- display
--!              |         |
--!  ctrl -------|         |
--!              |         |
--!              +---------+
--! 
--! \endverbatim
--!
--! Function 1:
--! -----------
--! With this function Switches 0 to 3 are used as input and will
--! be connected to port "input". Switch 4 is used to switch the 
--! "dot" in the HEX display on and off. 
--! HEX0 is used to display the input values and is connected to 
--! port "display". See figure 3:
--!
--! \verbatim
--!
--! Figure 3: Layout DE0-CV.
--!                                         
--!      DE0-CV KEY, SW, LED, and HEX layout
--!                               
--!                                      
--!               7-segment displays (HEX)
--!              +---+---+---+---+---+---+
--!              |   |   |   |   |   |ZZZ|
--!              |   |   |   |   |   |ZZZ|
--!              | 8.| 8.| 8.| 8.| 8.|ZZZ|
--!              |   |   |   |   |   |ZZZ|
--!              |   |   |   |   |   |ZZZ|
--!              +---+---+---+---+---+---+
--!                                    0  
--!        
--!      Number =>  9 8 7 6 5 4 3 2 1 0
--!                +-+-+-+-+-+-+-+-+-+-+
--! Leds (LEDR) => | | | | | | | | | | |
--!                +-+-+-+-+-+-+-+-+-+-+
--!                                   
--!                +-+-+-+-+-+-+-+-+-+-+
--!                | | | | | | | | | | |   +--+--+--+--+  +--+  
--!                +-+-+-+-+-+-+-+-+-+-+   |##|##|##|##|  |##|  <= KEY
--! Switch (SW) => | | | | | |C|X|X|X|X|   +--+--+--+--+  +--+  
--!                +-+-+-+-+-+-+-+-+-+-+    3  2  1  0     4    <= Number
--!      Number =>            4 3 2 1 0     
--!
--!  X = port "input"
--!  C = port "ctrl"
--!  Z = port "display"
--!
--! \endverbatim
--!
--! Note on CONSTANTS
--! =================
--!
--! These constants are used to make the VHDL code more readable.
--! Beacuse we will create both numbers and alphanumeric characters in the 
--! display the characters are defined as CONSTANTS in the GENERIC part of the
--! entity. This will simplify maintaining the code because CONSTANTs can be 
--! defined once and reused many times.
--! 
--! The following constants are defined to generate alle required characters.
--! Each led in the HEX_display has been given a number for the purpose of 
--! identification. See figure 2.
--!
--! \verbatim
--!
--!  Figure 4: 7-segments lay-out:
--!  
--!        -0-
--!       |   |
--!       5   1
--!       |   |
--!        -6-
--!       |   |
--!       4   2
--!       |   |
--!        -3-  7 (dot)
--!  
--! \endverbatim
--!
--! All LEDs are grouped in a STD_LOGIC_VECTOR where the index number is
--! equal to the LED number. 
--!
--! Because the LEDs are contolled using inverted-logic we have to apply a
--! '1' to switch the LED off. 
------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
------------------------------------------------------------------------------
ENTITY sevenOut4Decoder is
-- Constants to enhance readability of the VHDL code:

--   GENERIC ( -- REMOVE this comment when adding a CONSTANT

   -- Implement here CONSTANT

--   ); -- REMOVE this comment when adding a CONSTANT
   
   PORT (
      input   : IN  STD_LOGIC_VECTOR(3 DOWNTO 0); --! 4-bit binary input
      ctrl    : IN  STD_LOGIC;                    --! Control bit to access special functions
      display : OUT STD_LOGIC_VECTOR(0 TO 6)      --! 7-signals to control leds in HEX-display
   );
   
END ENTITY sevenOut4Decoder;
------------------------------------------------------------------------------
ARCHITECTURE implementation OF sevenOut4Decoder IS
   
   -- add here signals to your descretion
   
BEGIN

   -- Step 1: Connect port "dot" to the dot-segment in the HEX display.

   -- Display decoders. This code is using "WITH - SELECT" to encode 6 segments on
   -- a HEX diplay. This code is using the CONSTANTS that are defined at GENERIC.

   -- Step 2: Implement here the multiplexer that will present the normal characters.
   
   -- Step 3: Implement here the multiplexter that will the extended characters.

   -- Step 4: Implement here the  selector of the normal characters and the 
   -- extended characters using the ctrl signal.


END ARCHITECTURE implementation;
------------------------------------------------------------------------------
